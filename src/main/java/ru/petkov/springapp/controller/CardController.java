/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ru.petkov.springapp.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.petkov.springapp.model.Card;
import ru.petkov.springapp.service.CardService;

/**
 *
 * @author denis
 */
@RestController
public class CardController {

    private CardService cards;

    @Autowired
    public CardController(CardService card) {
        this.cards = card;
    }

    @GetMapping("cards/list")
    public List<Card> getCards() {
        return cards.getAllCards();
    }
    
    
}
