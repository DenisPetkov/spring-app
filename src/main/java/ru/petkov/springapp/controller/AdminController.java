/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ru.petkov.springapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.petkov.springapp.model.Card;
import ru.petkov.springapp.service.CardService;
 
/**
 *
 * @author denis
 */
@RestController
//@RequestMapping("/admin")
public class AdminController {

    private CardService cards;

    @Autowired
    public AdminController(CardService card) {
        this.cards = card;
    }

    @PostMapping("/card/delete/{id}")
    public String deleteCard(@PathVariable Long id) {
        Card orElse = cards.getAllCards().stream().filter(card -> card.getId().equals(id)).findFirst().orElse(null);
        cards.remove(orElse);
        return "Card deleted ";
    }

    @GetMapping("admin/card/by/{id}")
    public Card getCardById(@PathVariable Long id) {
        return cards.getAllCards().stream().filter(card -> card.getId().equals(id)).findFirst().orElse(null);
    }

    @PostMapping("admin/card/add")
    public String addCard(@RequestBody Card card) {
        cards.add(card);
        return "Card added";
    }
}
