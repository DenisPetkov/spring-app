/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ru.petkov.springapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 *
 * @author denis
 */
@RestController
public class MainController {

    private final ModelAndView view = new ModelAndView();

    @GetMapping("/")
    public ModelAndView getHomePage() {
        view.setViewName("home.html");
        return view;
    }

    @GetMapping("/home")
    public ModelAndView getMainPage() {
        view.setViewName("home.html");
        return view;
    }

    @GetMapping("/login")
    public ModelAndView getFormLogin() {
        view.setViewName("login.html");
        return view;
    }

    @GetMapping("/admin")
    public ModelAndView getAdminPage() {
        view.setViewName("admin.html");
        return view;
    }
  
    @GetMapping("/admin/create")
    public ModelAndView getCreateCardPage() {
        view.setViewName("createCard.html");
        return view;
    }
}
