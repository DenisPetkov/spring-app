/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ru.petkov.springapp.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

/**
 *
 * @author denis
 */
@Component
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Card {
   
    private Long id;
    private String name;
    private String description;
    private Integer price;
    
}
