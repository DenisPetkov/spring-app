/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ru.petkov.springapp.service;

import java.util.List;
import org.springframework.stereotype.Service;
import ru.petkov.springapp.model.Card;

/**
 *
 * @author denis
 */
@Service
public class CardService {

    List<Card> cards = List.of(new Card(100L, "Easy", "Easy - very fast room", 1000),
            new Card(101L, "Medium", "Medium - Long lasting room", 4000),
            new Card(103L, "Hard", "Hard - we love hardcore..", 9000));

    public List<Card> getAllCards() {
        return cards;
    }

    public void add(Card card) {
        cards.add(card);
    }

    public void remove(Card card) {
        cards.remove(card);
    }
}
